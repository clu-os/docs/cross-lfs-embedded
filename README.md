# Cross Linux From Scratch - Embedded

Read the latest [CI/CD](https://en.wikipedia.org/wiki/CI/CD) version online:

- [HTML (separate pages)](https://clu-os.gitlab.io/docs/cross-lfs-embedded/): [arm], [mips], [x86]
- HTML (single page): [arm.xhtml], [mips.xhtml], [x86.xhtml]
- PDF

Changes since the last official release:

- [x] [Link relationships](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/rel) have been re-enabled (`prev`, `next`, `up`, `home`, `license`)
- [x] [Open Graph](https://ogp.me/) metadata has been added, social timeline cards support (`<meta property="og:*">`)
- [x] Support for configurable filename extension has been re-enabled ([html.ext](https://docbook.sourceforge.net/release/xsl/current/doc/html/html.ext.html))
- [ ] Support for configurable output character encoding ([chunker.output.encoding](https://docbook.sourceforge.net/release/xsl/current/doc/html/chunker.output.encoding.html))
- [x] Some filenames are based on element `id` ([use.id.as.filename](https://docbook.sourceforge.net/release/xsl/current/doc/html/use.id.as.filename.html))
- [ ] Parallel `make`, `WIP`
- [ ] Incremental `make`, `WIP`
- [x] Same `make` targets for all books (`html`, `pdf`, `nochunks`, ...)
- [x] CSS stylesheets have been combined, for all media (`screen` and `print`)
- [ ] "Dark Mode", `TODO`

Visit the official, upstream, pages:

- [Cross-Linux From Scratch on GitHub](https://github.com/cross-lfs)
- [CLFS Trac](https://trac.clfs.org/)



[arm]:   https://clu-os.gitlab.io/docs/cross-lfs-embedded/arm/
[mips]:  https://clu-os.gitlab.io/docs/cross-lfs-embedded/mips/
[x86]:   https://clu-os.gitlab.io/docs/cross-lfs-embedded/x86/

[arm.xhtml]:   https://clu-os.gitlab.io/docs/cross-lfs-embedded/CLFS-arm.xhtml
[mips.xhtml]:  https://clu-os.gitlab.io/docs/cross-lfs-embedded/CLFS-mips.xhtml
[x86.xhtml]:   https://clu-os.gitlab.io/docs/cross-lfs-embedded/CLFS-x86.xhtml

Original (almost) README below:
___

Cross Linux From Scratch Embedded
=================================

Cross Linux From Scratch (CLFS) Embedded shows you how to build a Linux system
from source, targeting users who want to make a very small system.

Running with 16 MB of RAM and 16 MB of "disk" is very possible.

For more information about CLFS, visit the [CLFS website][trac].

To build the book, please see the BOOK/INSTALL.md file for requirements and
exact steps.

[trac]:  http://trac.clfs.org
